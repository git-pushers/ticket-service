
generate-service:
	protoc --go_out pkg --go_opt paths=source_relative \
		-I api \
	  --go-grpc_out pkg --go-grpc_opt=paths=source_relative \
	  --grpc-gateway_out pkg --grpc-gateway_opt paths=source_relative  \
	  --openapiv2_out api \
	  api/performance/performance.proto

generate-message:
	protoc -I api --go_out pkg --go_opt paths=source_relative api/performance/message.proto


